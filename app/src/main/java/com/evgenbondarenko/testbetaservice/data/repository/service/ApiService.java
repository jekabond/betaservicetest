package com.evgenbondarenko.testbetaservice.data.repository.service;

import com.evgenbondarenko.testbetaservice.data.model.response.authentication.RegistrationBody;
import com.evgenbondarenko.testbetaservice.data.model.response.signals.ResponseSignals;
import java.util.List;
import io.reactivex.Single;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @POST("api/Authentication/RequestMoblieCabinetApiToken")
    Single<String> authentication(@Body RegistrationBody body);

    @GET("clientmobile/GetAnalyticSignals/{params}")
    Single<List<ResponseSignals>> getSignals(@Path("params") String params,
                                             @Header("passkey") String token,
                                             @Query(value = "tradingsystem", encoded = false) int tradingsystem,
                                             @Query(value = "pairs", encoded = false) String pairs,
                                             @Query(value = "from", encoded = false) Long timeFrom,
                                             @Query(value = "to", encoded = false) Long timeBy);
}
