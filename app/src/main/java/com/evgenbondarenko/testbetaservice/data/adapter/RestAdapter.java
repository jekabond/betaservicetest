package com.evgenbondarenko.testbetaservice.data.adapter;


import com.evgenbondarenko.testbetaservice.BuildConfig;
import com.evgenbondarenko.testbetaservice.Constant;
import com.evgenbondarenko.testbetaservice.data.repository.service.ApiService;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestAdapter {

    private ApiService apiService;

    private RestAdapter() {
        apiService = api(restAdapter(gson(), okHttpClient()));
    }

    private static class SingletonHolder {
        private final static RestAdapter instance = new RestAdapter();
    }

    public static RestAdapter getInstance() {
        return SingletonHolder.instance;
    }

    private OkHttpClient okHttpClient() {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(logging);
        }
        builder.connectTimeout(60 * 1000, TimeUnit.MILLISECONDS)
                .readTimeout(60 * 1000, TimeUnit.MILLISECONDS);

        return builder.build();
    }

    private Gson gson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder
                .setLenient()
                .create();
    }

    private Retrofit restAdapter(Gson gson, OkHttpClient okHttpClient) {
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.client(okHttpClient)
                .baseUrl(Constant.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson));
        return builder.build();
    }

    private ApiService api(Retrofit restAdapter) {
        return restAdapter.create(ApiService.class);
    }

    public ApiService getApiService() {
        return apiService;
    }
}
