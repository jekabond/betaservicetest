package com.evgenbondarenko.testbetaservice.data.repository.remote_storage;

import com.evgenbondarenko.testbetaservice.data.adapter.RestAdapter;
import com.evgenbondarenko.testbetaservice.data.model.response.authentication.RegistrationBody;
import com.evgenbondarenko.testbetaservice.data.model.response.signals.ResponseSignals;
import java.util.List;
import io.reactivex.Single;


public class RemoteStorage  implements RemoteStorageContract {

    @Override
    public Single<String> authentication(RegistrationBody body) {
        return RestAdapter.getInstance().getApiService().authentication(body);
    }

    @Override
    public Single<List<ResponseSignals>> getSignal(String login, String token, int tradingsystem, String pair, Long timeFrom, Long timeBy) {
        return RestAdapter.getInstance().getApiService().getSignals(login, token, tradingsystem, pair, timeFrom, timeBy);
    }
}
