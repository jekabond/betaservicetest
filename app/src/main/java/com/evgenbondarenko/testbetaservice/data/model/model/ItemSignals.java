package com.evgenbondarenko.testbetaservice.data.model.model;


import com.evgenbondarenko.testbetaservice.data.model.response.signals.ResponseSignals;

public class ItemSignals {

    private Long mActualTime;

    private String mComment;

    private String mPair;

    private Double mPrice;

    private Double mSl;

    private Double mTp;

    public ItemSignals(ResponseSignals signals) {
        this.mActualTime = signals.getActualTime();
        this.mComment = signals.getComment();
        this.mPair = signals.getPair();
        this.mPrice = signals.getPrice();
        this.mSl = signals.getSl();
        this.mTp = signals.getTp();
    }

    public Long getmActualTime() {
        return mActualTime;
    }

    public String getmComment() {
        return mComment;
    }

    public String getmPair() {
        return mPair;
    }

    public Double getmPrice() {
        return mPrice;
    }

    public Double getmSl() {
        return mSl;
    }

    public Double getmTp() {
        return mTp;
    }
}
