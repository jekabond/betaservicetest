package com.evgenbondarenko.testbetaservice.data.repository;



import com.evgenbondarenko.testbetaservice.data.model.response.authentication.RegistrationBody;
import com.evgenbondarenko.testbetaservice.data.model.response.signals.ResponseSignals;
import java.util.List;
import io.reactivex.Single;


public interface RepositoryContract {
    interface Authorization {
        Single<String> authentication(RegistrationBody body);

        void addToken(String login, String token);
    }

    interface Signals {
        Single<List<ResponseSignals>> getSignal(String pair);

        String getToken();

        String getLogin();
    }
}
