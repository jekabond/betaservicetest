package com.evgenbondarenko.testbetaservice.data.repository.remote_storage;

import com.evgenbondarenko.testbetaservice.data.model.response.authentication.RegistrationBody;
import com.evgenbondarenko.testbetaservice.data.model.response.signals.ResponseSignals;
import java.util.List;
import io.reactivex.Single;


public interface RemoteStorageContract {
    Single authentication(RegistrationBody body);

    Single<List<ResponseSignals>> getSignal(String login, String token, int tradingsystem, String pair, Long timeFrom, Long timeBy);
}
