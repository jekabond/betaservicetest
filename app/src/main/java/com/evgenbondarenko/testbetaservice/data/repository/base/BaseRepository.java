package com.evgenbondarenko.testbetaservice.data.repository.base;

import io.reactivex.Scheduler;
import io.reactivex.SingleTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public abstract class BaseRepository {
    private Scheduler jobThread;
    private Scheduler observeThread;

    public BaseRepository() {
        jobThread = Schedulers.io();
        observeThread = AndroidSchedulers.mainThread();
    }

    protected <T> SingleTransformer<T, T> applySingleSchedulers() {
        return single -> single.subscribeOn(jobThread)
                .observeOn(observeThread);
    }
}
