package com.evgenbondarenko.testbetaservice.data.repository;

import android.content.Context;
import com.evgenbondarenko.testbetaservice.Constant;
import com.evgenbondarenko.testbetaservice.data.model.response.authentication.RegistrationBody;
import com.evgenbondarenko.testbetaservice.data.model.response.signals.ResponseSignals;
import com.evgenbondarenko.testbetaservice.data.repository.base.BaseRepository;
import com.evgenbondarenko.testbetaservice.data.repository.local_storage.LocaleStorage;
import com.evgenbondarenko.testbetaservice.data.repository.remote_storage.RemoteStorage;
import com.evgenbondarenko.testbetaservice.data.repository.remote_storage.RemoteStorageContract;

import java.util.List;

import io.reactivex.Single;


public class Repository extends BaseRepository implements RepositoryContract.Authorization, RepositoryContract.Signals {

    private LocaleStorage localeStorage;
    private RemoteStorageContract remoteStorage;


    public Repository(Context context) {
        localeStorage = LocaleStorage.getInstance(context);
        remoteStorage = new RemoteStorage();
    }

    @Override
    public Single<String> authentication(RegistrationBody body) {
        return remoteStorage.authentication(body)
                .compose(applySingleSchedulers());
    }

    @Override
    public void addToken(String login, String token) {
        localeStorage.addToken(login, token);
    }

    @Override
    public Single<List<ResponseSignals>> getSignal(String pair) {
        return remoteStorage.getSignal(getLogin(), getToken(), Constant.TRADINGSYSTEM, pair, Constant.TIME_FROM, Constant.TIME_TO)
                .compose(applySingleSchedulers());
    }

    @Override
    public String getToken() {
        return localeStorage.getToken();
    }

    @Override
    public String getLogin() {
        return localeStorage.getLogin();
    }
}
