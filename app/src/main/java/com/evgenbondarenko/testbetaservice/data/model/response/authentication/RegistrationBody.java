package com.evgenbondarenko.testbetaservice.data.model.response.authentication;

public class RegistrationBody {
    private String login;
    private String password;

    public RegistrationBody(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
