
package com.evgenbondarenko.testbetaservice.data.model.response.signals;

import com.google.gson.annotations.SerializedName;

public class ResponseSignals {

    @SerializedName("ActualTime")
    private Long mActualTime;
    @SerializedName("Cmd")
    private Long mCmd;
    @SerializedName("Comment")
    private String mComment;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("Pair")
    private String mPair;
    @SerializedName("Period")
    private String mPeriod;
    @SerializedName("Price")
    private Double mPrice;
    @SerializedName("Sl")
    private Double mSl;
    @SerializedName("Tp")
    private Double mTp;
    @SerializedName("TradingSystem")
    private Long mTradingSystem;


    @Override
    public String toString() {
        return "ResponseSignals{" +
                "mActualTime=" + mActualTime +
                ", mCmd=" + mCmd +
                ", mComment='" + mComment + '\'' +
                ", mId=" + mId +
                ", mPair='" + mPair + '\'' +
                ", mPeriod='" + mPeriod + '\'' +
                ", mPrice=" + mPrice +
                ", mSl=" + mSl +
                ", mTp=" + mTp +
                ", mTradingSystem=" + mTradingSystem +
                '}';
    }

    public Long getActualTime() {
        return mActualTime;
    }

    public void setActualTime(Long ActualTime) {
        mActualTime = ActualTime;
    }

    public Long getCmd() {
        return mCmd;
    }

    public void setCmd(Long Cmd) {
        mCmd = Cmd;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String Comment) {
        mComment = Comment;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long Id) {
        mId = Id;
    }

    public String getPair() {
        return mPair;
    }

    public void setPair(String Pair) {
        mPair = Pair;
    }

    public String getPeriod() {
        return mPeriod;
    }

    public void setPeriod(String Period) {
        mPeriod = Period;
    }

    public Double getPrice() {
        return mPrice;
    }

    public void setPrice(Double Price) {
        mPrice = Price;
    }

    public Double getSl() {
        return mSl;
    }

    public void setSl(Double Sl) {
        mSl = Sl;
    }

    public Double getTp() {
        return mTp;
    }

    public void setTp(Double Tp) {
        mTp = Tp;
    }

    public Long getTradingSystem() {
        return mTradingSystem;
    }

    public void setTradingSystem(Long TradingSystem) {
        mTradingSystem = TradingSystem;
    }

}
