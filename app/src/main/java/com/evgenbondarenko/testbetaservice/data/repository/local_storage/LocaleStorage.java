package com.evgenbondarenko.testbetaservice.data.repository.local_storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import static android.content.Context.MODE_PRIVATE;

public class LocaleStorage {

    private SharedPreferences pref;
    private Editor editor;
    private final static String STORAGE_NAME = "SharedToken";
    private final String KEY_NAME = "token", KEY_LOGIN = "login", KEY_PASSWORD = "password";
    private static Context mContext;

    private LocaleStorage() {
        pref = mContext.getApplicationContext().getSharedPreferences(STORAGE_NAME, MODE_PRIVATE);
    }

    private static class SingletonHolder {
        private final static LocaleStorage instance = new LocaleStorage();
    }

    public static LocaleStorage getInstance(Context context) {
        mContext = context;
        return SingletonHolder.instance;
    }

    public void addToken(String login, String token) {
        editor = pref.edit();
        editor.putString(KEY_NAME, token);
        editor.putString(KEY_LOGIN, login);
        editor.apply();
    }

    public String getToken() {
        if (pref != null) return pref.getString(KEY_NAME, null);
        else return null;
    }

    public String getLogin() {
        if (pref != null) return pref.getString(KEY_LOGIN, null);
        else return null;
    }
}
