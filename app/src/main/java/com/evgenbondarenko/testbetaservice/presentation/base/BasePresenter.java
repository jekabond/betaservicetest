package com.evgenbondarenko.testbetaservice.presentation.base;


public interface BasePresenter <V> {
    void attachView(V view);

    void detachView();
}
