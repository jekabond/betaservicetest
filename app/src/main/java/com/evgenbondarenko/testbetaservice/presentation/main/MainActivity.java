package com.evgenbondarenko.testbetaservice.presentation.main;


import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.widget.Toast;

import com.evgenbondarenko.testbetaservice.R;
import com.evgenbondarenko.testbetaservice.data.model.model.ItemSignals;
import com.evgenbondarenko.testbetaservice.databinding.ActivityMainBinding;
import com.evgenbondarenko.testbetaservice.presentation.base.BaseActivity;
import com.evgenbondarenko.testbetaservice.presentation.base.BasePresenter;
import com.evgenbondarenko.testbetaservice.presentation.main.adapter.PairsSignalsAdapter;
import com.evgenbondarenko.testbetaservice.presentation.main.adapter.SignalsAdapter;

import java.util.List;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements MainContract.View {

    private MainContract.Presenter presenter;
    private PairsSignalsAdapter pairsSignalsAdapter;
    private SignalsAdapter signalsAdapter;
    private RecyclerView.LayoutManager layoutManagerPair, layoutManagerSignal;

    @Override
    protected void initView() {
        bindView(R.layout.activity_main);
        presenter = new MainPresenter(this);
        getBinding().setEvent(presenter);
        displayPair();
    }

    @Override
    public BasePresenter getPresenter() {
        return presenter;
    }

    private void displayPair() {
        layoutManagerPair = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        getBinding().rvSignalPair.setLayoutManager(layoutManagerPair);
        pairsSignalsAdapter = new PairsSignalsAdapter(presenter);
        getBinding().rvSignalPair.setAdapter(pairsSignalsAdapter);
    }

    @SuppressLint("WrongConstant")
    @Override
    public void displaySignal(List<ItemSignals> itemSignals) {
        getBinding().rvSignals.setNestedScrollingEnabled(false);
        layoutManagerSignal = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        getBinding().rvSignals.setLayoutManager(layoutManagerSignal);
        signalsAdapter = new SignalsAdapter(itemSignals);
        getBinding().rvSignals.setAdapter(signalsAdapter);
    }

    @Override
    public void updatePositionPair(int position) {
        pairsSignalsAdapter.updateItem(position);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attachView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.detachView();
    }


    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}