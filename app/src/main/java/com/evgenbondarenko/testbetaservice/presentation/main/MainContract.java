package com.evgenbondarenko.testbetaservice.presentation.main;

import com.evgenbondarenko.testbetaservice.data.model.model.ItemSignals;
import com.evgenbondarenko.testbetaservice.presentation.base.BasePresenter;

import java.util.List;


public interface MainContract {

    interface View {
        void displaySignal(List<ItemSignals> list);

        void updatePositionPair(int position);

        void showMessage(String message);
    }

    interface Presenter extends BasePresenter<View> {
        void updatePositionPair(int position);

        void onClickResult();
    }
}
