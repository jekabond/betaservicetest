package com.evgenbondarenko.testbetaservice.presentation.main.adapter;

import android.content.Context;

import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.evgenbondarenko.testbetaservice.R;
import com.evgenbondarenko.testbetaservice.data.model.model.ItemSignals;
import com.evgenbondarenko.testbetaservice.databinding.ListItemSignalsBinding;


public class SignalsViewHolder extends RecyclerView.ViewHolder{

    private ListItemSignalsBinding mBinding;
    private Context mContext;

    public SignalsViewHolder(Context context, View itemView) {
        super(itemView);
        mContext = context;
        mBinding = DataBindingUtil.bind(itemView);
    }

    public void bind(ItemSignals item){
        if(item != null){
          mBinding.tvActualTime.setText(mContext.getResources().getString(R.string.actualTime).concat(String.valueOf(item.getmActualTime())));
          mBinding.tvComment.setText(mContext.getResources().getString(R.string.comment).concat(item.getmComment()));
          mBinding.tvPair.setText(mContext.getResources().getString(R.string.pair).concat(item.getmPair()));
          mBinding.tvPrice.setText(mContext.getResources().getString(R.string.price).concat(String.valueOf(item.getmPrice())));
          mBinding.tvSl.setText(mContext.getResources().getString(R.string.sl).concat(String.valueOf(item.getmSl())));
          mBinding.tvTp.setText(mContext.getResources().getString(R.string.tp).concat(String.valueOf(item.getmTp())));
        }
    }
}
