package com.evgenbondarenko.testbetaservice.presentation.login;

import android.content.Context;
import com.evgenbondarenko.testbetaservice.data.model.response.authentication.RegistrationBody;
import com.evgenbondarenko.testbetaservice.data.repository.Repository;
import com.evgenbondarenko.testbetaservice.data.repository.RepositoryContract;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


public class LoginPresenter implements LoginContract.Presenter {

    private LoginContract.View mView;
    private RepositoryContract.Authorization repository;
    private final CompositeDisposable mCompositeDisposable;
    private String mLogin;


    public LoginPresenter(Context context) {
        mCompositeDisposable = new CompositeDisposable();
        repository = new Repository(context);
    }

    @Override
    public void attachView(LoginContract.View view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.dispose();
        }
        repository = null;
    }

    @Override
    public void onClickOk() {
        mView.getData();
    }

    @Override
    public void onClickCancel() {
        mView.singCancel();
    }

    @Override
    public void checkData(String login, String password) {
        if (login != null && password != null && !login.isEmpty() && !password.isEmpty()) {
            authentication(new RegistrationBody(mLogin = login, password));
        }
    }

    private void authentication(RegistrationBody body) {
        Disposable disposable = repository.authentication(body)
                .doOnError(Throwable::getMessage)
                .subscribe(this::getToken, throwable -> mView.showMessage("No login || password"));
        mCompositeDisposable.add(disposable);
    }

    private void getToken(String token) {
        if (token != null && !token.isEmpty()) {
            repository.addToken(mLogin, token);
            mView.singInOk();
        } else {
            mView.showMessage("incorrect data");
        }
    }

}
