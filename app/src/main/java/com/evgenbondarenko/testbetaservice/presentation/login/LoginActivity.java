package com.evgenbondarenko.testbetaservice.presentation.login;

import android.content.Intent;
import android.widget.Toast;
import com.evgenbondarenko.testbetaservice.R;
import com.evgenbondarenko.testbetaservice.databinding.ActivityLoginBinding;
import com.evgenbondarenko.testbetaservice.presentation.base.BaseActivity;
import com.evgenbondarenko.testbetaservice.presentation.base.BasePresenter;
import com.evgenbondarenko.testbetaservice.presentation.main.MainActivity;

public class LoginActivity extends BaseActivity<ActivityLoginBinding> implements LoginContract.View {

    private LoginContract.Presenter presenter;

    @Override
    protected void initView() {
        bindView(R.layout.activity_login);
        presenter = new LoginPresenter(this);
        getBinding().setEvent(presenter);
    }

    @Override
    public BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attachView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.detachView();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void singInOk() {
        Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
        LoginActivity.this.startActivity(mainIntent);
        LoginActivity.this.finish();
    }

    @Override
    public void getData() {
        presenter.checkData(getBinding().etLogin.getText().toString(), getBinding().etPassword.getText().toString());
    }

    @Override
    public void singCancel() {
        this.finish();
    }

}

