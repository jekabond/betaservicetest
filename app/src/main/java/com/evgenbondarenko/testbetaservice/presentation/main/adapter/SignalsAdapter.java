package com.evgenbondarenko.testbetaservice.presentation.main.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.evgenbondarenko.testbetaservice.R;
import com.evgenbondarenko.testbetaservice.data.model.model.ItemSignals;

import java.util.List;


public class SignalsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ItemSignals> mList;

    public SignalsAdapter(List<ItemSignals> list) {
        mList = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_signals, parent, false);
        RecyclerView.ViewHolder viewHolder = new SignalsViewHolder(parent.getContext(), view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((SignalsViewHolder) holder).bind(mList.get(position));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
