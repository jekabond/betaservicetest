package com.evgenbondarenko.testbetaservice.presentation.main.adapter.model_pair;


import java.util.ArrayList;
import java.util.List;

public class ArrayPairConst {
    private List<ItemPair> list;

    private ArrayPairConst() {
       list = new ArrayList<>();
       addList();
    }

    private void addList() {
        list.add(new ItemPair("EURUSD",false));
        list.add(new ItemPair("GBPUSD",false));
        list.add(new ItemPair("USDJPY",false));
        list.add(new ItemPair("USDCHF",false));
        list.add(new ItemPair("USDCAD",false));
        list.add(new ItemPair("AUDUSD",false));
        list.add(new ItemPair("NZDUSD",false));
    }

    private static class SingletonHolder{
        private final static ArrayPairConst instance = new ArrayPairConst();
    }

    public static ArrayPairConst getInstance(){
        return SingletonHolder.instance;
    }

    public List<ItemPair> getList() {
        return list;
    }

    public class ItemPair{
        private String namePair;
        private boolean isSelected;

        public ItemPair(String namePair, boolean isSelected) {
            this.namePair = namePair;
            this.isSelected = isSelected;
        }

        public String getNamePair() {
            return namePair;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }
    }
}
