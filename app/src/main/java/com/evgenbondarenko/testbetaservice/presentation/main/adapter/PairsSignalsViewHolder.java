package com.evgenbondarenko.testbetaservice.presentation.main.adapter;

import android.content.Context;
import android.view.View;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.evgenbondarenko.testbetaservice.R;
import com.evgenbondarenko.testbetaservice.databinding.ListItemPairBinding;
import com.evgenbondarenko.testbetaservice.presentation.main.MainContract;
import com.evgenbondarenko.testbetaservice.presentation.main.adapter.model_pair.ArrayPairConst;


public class PairsSignalsViewHolder extends RecyclerView.ViewHolder {
    private MainContract.Presenter mEventListener;
    private ListItemPairBinding mBinding;
    private Context mContext;

    public PairsSignalsViewHolder(Context context, View itemView, MainContract.Presenter eventListener) {
        super(itemView);
        mContext = context;
        mEventListener = eventListener;
        mBinding = DataBindingUtil.bind(itemView);
        mBinding.setEvent(mEventListener);
    }

    public void bind(ArrayPairConst.ItemPair item, int position) {
        if (item != null) {
            mBinding.tvItemPair.setText(item.getNamePair());
            mBinding.tvItemPair.setBackgroundColor(item.isSelected() ?
                    mContext.getResources().getColor(R.color.selected_item_pair) :
                    mContext.getResources().getColor(R.color.no_selected_item_pair));
            mBinding.tvItemPair.setOnClickListener(v -> {
                item.setSelected(item.isSelected() ? false : true);
                mEventListener.updatePositionPair(position);
            });
        }
    }
}
