package com.evgenbondarenko.testbetaservice.presentation.base;


import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;


public abstract class BaseActivity<Binding extends ViewDataBinding>  extends AppCompatActivity {

    private Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }


    protected void bindView(int layoutId) {
        binding = DataBindingUtil.setContentView(this, layoutId);
    }

    public Binding getBinding() {
        return binding;
    }

    public abstract BasePresenter getPresenter();

    @Override
    protected void onStart() {
        super.onStart();
    }

    protected abstract void initView();

    @Override
    public void onDestroy() {
        if (getPresenter() != null) {
            getPresenter().detachView();
        }
        super.onDestroy();
    }
}
