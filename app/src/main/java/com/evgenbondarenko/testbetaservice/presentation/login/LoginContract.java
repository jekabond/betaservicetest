package com.evgenbondarenko.testbetaservice.presentation.login;

import com.evgenbondarenko.testbetaservice.presentation.base.BasePresenter;

public interface LoginContract {
    interface View {
        void showMessage(String message);

        void singInOk();

        void getData();

        void singCancel();
    }

    interface Presenter extends BasePresenter<View> {
        void onClickOk();

        void onClickCancel();

        void checkData(String login, String password);
    }
}
