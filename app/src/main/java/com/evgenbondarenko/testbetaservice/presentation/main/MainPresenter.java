package com.evgenbondarenko.testbetaservice.presentation.main;

import android.content.Context;

import com.evgenbondarenko.testbetaservice.data.model.model.ItemSignals;
import com.evgenbondarenko.testbetaservice.data.model.response.signals.ResponseSignals;
import com.evgenbondarenko.testbetaservice.data.repository.Repository;
import com.evgenbondarenko.testbetaservice.data.repository.RepositoryContract;
import com.evgenbondarenko.testbetaservice.presentation.main.adapter.model_pair.ArrayPairConst;
import com.evgenbondarenko.testbetaservice.presentation.main.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


public class MainPresenter implements MainContract.Presenter {

    private MainContract.View mView;
    private RepositoryContract.Signals repository;
    private final CompositeDisposable mCompositeDisposable;
    private String pair;
    private List<ItemSignals> listSignal;


    public MainPresenter(Context context) {
        mCompositeDisposable = new CompositeDisposable();
        repository = new Repository(context);
        listSignal = new ArrayList<>();
    }


    @Override
    public void attachView(MainContract.View view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.dispose();
        }
        repository = null;
    }

    @Override
    public void updatePositionPair(int position) {
        mView.updatePositionPair(position);
    }


    @Override
    public void onClickResult() {
        if (checkPair()) {
            getSignal();
        } else {
            mView.showMessage("Select pair signals");
        }
    }

    private boolean checkPair() {
        pair = null;
        List<String> listPair = new ArrayList<>();
        for (ArrayPairConst.ItemPair obj : ArrayPairConst.getInstance().getList()) {
            if (obj.isSelected()) {
                listPair.add(obj.getNamePair());
            }
        }
        if (listPair.size() != 0) {
            pair = StringUtils.getInstance().StringFormat(listPair);
        }
        return pair != null ? true : false;
    }

    private void getSignal() {
        Disposable disposable = repository.getSignal(pair)
                .doOnError(Throwable::getMessage)
                .subscribe(this::getSignalResponse, throwable -> mView.showMessage("no data"));
        mCompositeDisposable.add(disposable);
    }

    private void getSignalResponse(List<ResponseSignals> list) {
        if (list != null && list.size() != 0) {
            convertResponse(list);
            mView.displaySignal(listSignal);
        } else {
            mView.showMessage("not data");
        }
    }

    private void convertResponse(List<ResponseSignals> list) {
        listSignal.clear();
        for (ResponseSignals obj : list) {
            listSignal.add(new ItemSignals(obj));
        }
    }
}
