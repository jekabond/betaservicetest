package com.evgenbondarenko.testbetaservice.presentation.main.util;

import java.util.List;

public class StringUtils {

    private StringUtils() {

    }

    private static class SingletonHolder {
        private final static StringUtils instance = new StringUtils();
    }

    public static StringUtils getInstance() {
        return StringUtils.SingletonHolder.instance;
    }


    public String StringFormat(List<String> list) {
        return list.toString().replaceAll("[\\[.\\].\\s+]", "");
    }
}
