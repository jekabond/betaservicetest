package com.evgenbondarenko.testbetaservice.presentation.main.adapter;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.evgenbondarenko.testbetaservice.R;
import com.evgenbondarenko.testbetaservice.presentation.main.MainContract;
import com.evgenbondarenko.testbetaservice.presentation.main.adapter.model_pair.ArrayPairConst;


public class PairsSignalsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private MainContract.Presenter mEventListener;
    private ArrayPairConst arrayPairConst = ArrayPairConst.getInstance();


    public PairsSignalsAdapter(MainContract.Presenter mEventListener) {
        this.mEventListener = mEventListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.list_item_pair, parent, false);
        RecyclerView.ViewHolder viewHolder = new PairsSignalsViewHolder(parent.getContext(),view, mEventListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((PairsSignalsViewHolder)holder).bind(arrayPairConst.getList().get(position),position);
    }

    public void updateItem(int position){
        notifyItemChanged(position);
    }

    @Override
    public int getItemCount() {
        return arrayPairConst.getList().size();
    }
}
